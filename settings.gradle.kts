rootProject.name = "quote-service-backend"

// Here, we declare libraries and their versions to use
// To make the dependency version management simpler
// https://docs.gradle.org/current/userguide/platforms.html
dependencyResolutionManagement {
    versionCatalogs {
        create("libs") {
            val javaxAnnotationVersion = version("javax-annotation", "1.3.2")
            val javaxValidationVersion = version("javax-validation", "1.1.0.Final")

            val jacksonVersion = version("jackson", "2.15.3")
            val h2Version = version("h2", "2.2.224")
            val h2R2DBCVersion = version("h2-r2dbc", "1.0.0.RELEASE")
            val lombokVersion = version("lombok-plugin", "8.4")
            val springVersion = version("spring", "6.1.3")
            val springBootVersion = version("spring-boot", "3.2.2")
            val springDocVersion = version("spring-doc", "2.3.0")
            val openApiCodegenVersion = version("codegen-openapi", "7.2.0")
            val reactorVersion = version("reactor", "3.6.2")

            // Spring Boot
            library("spring-boot", "org.springframework.boot", "spring-boot-starter-webflux").versionRef(springBootVersion)
            library("spring-boot-data.r2dbc", "org.springframework.boot", "spring-boot-starter-data-r2dbc").versionRef(springBootVersion)
            library("spring-boot-actuator", "org.springframework.boot", "spring-boot-starter-actuator").versionRef(springBootVersion)
            plugin("spring-boot", "org.springframework.boot").versionRef(springBootVersion)

            library("spring-web", "org.springframework", "spring-web").versionRef(springVersion)
            library("spring-webflux", "org.springframework", "spring-webflux").versionRef(springVersion)
            library("spring-context", "org.springframework", "spring-context").versionRef(springVersion)

            library("spring-doc", "org.springdoc", "springdoc-openapi-starter-webflux-ui").versionRef(springDocVersion)

            // OpenAPI Codegen
            plugin("codegen-openapi", "org.openapi.generator").versionRef(openApiCodegenVersion)

            // Reactor Core
            library("reactor-core", "io.projectreactor", "reactor-core").versionRef(reactorVersion)

            // H2
            library("h2-r2dbc", "io.r2dbc", "r2dbc-h2").versionRef(h2R2DBCVersion)
            library("h2", "com.h2database", "h2").versionRef(h2Version)

            library("jackson-annotations", "com.fasterxml.jackson.core", "jackson-annotations").versionRef(jacksonVersion)
            library("jackson-core", "com.fasterxml.jackson.core", "jackson-core").versionRef(jacksonVersion)
            library("jackson-databind", "com.fasterxml.jackson.core", "jackson-databind").versionRef(jacksonVersion)
            library("jackson-jsr310", "com.fasterxml.jackson.datatype", "jackson-datatype-jsr310").versionRef(jacksonVersion)

            // Lombok
            plugin("lombok-plugin", "io.freefair.lombok").versionRef(lombokVersion)

            library("javax-annotation", "javax.annotation", "javax.annotation-api").versionRef(javaxAnnotationVersion)
            library("javax-validation", "javax.validation", "validation-api").versionRef(javaxValidationVersion)
            library("javax-jsr305", "com.google.code.findbugs", "jsr305").version("3.0.0")

            bundle("h2-runtime", listOf("h2", "h2-r2dbc"))
            bundle("jackson", listOf("jackson-annotations", "jackson-core", "jackson-databind", "jackson-jsr310"))
            bundle("codegen-workaround", listOf("javax-annotation", "javax-validation", "javax-jsr305"))
        }
    }
}

include("domain")
include("domain:model")
include("domain:usecase")
include("domain:user-stories")

include("application")
include("application:services")
include("application:adapters")

include("infrastructure")
include("infrastructure:quote-source-quotable")
include("infrastructure:quote-source-dummyjson")
include("infrastructure:quote-source-statham")
include("infrastructure:quote-source-composite")
include("infrastructure:likes-storage")
include("infrastructure:database-h2")

include("presentation")
include("presentation:random-query-web")
include("presentation:health-actuator")
include("presentation:quote-service-web-api")
include("presentation:likes-api")

include("deployment:app")
