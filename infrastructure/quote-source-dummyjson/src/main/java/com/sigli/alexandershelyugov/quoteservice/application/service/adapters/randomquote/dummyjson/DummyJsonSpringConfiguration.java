package com.sigli.alexandershelyugov.quoteservice.application.service.adapters.randomquote.dummyjson;

import com.sigli.alexandershelyugov.quoteservice.adapters.randomquote.dummyjson.api.QuoteApi;
import com.sigli.alexandershelyugov.quoteservice.adapters.randomquote.dummyjson.invoker.ApiClient;
import com.sigli.alexandershelyugov.quoteservice.application.service.adapters.randomquote.GetRandomQuotePort;
import lombok.val;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class DummyJsonSpringConfiguration {
    @Bean("dummyJsonRandomQuote")
    public GetRandomQuotePort getRandomQuotePort(QuoteApi quoteApi) {
        return new DummyJsonRandomQuotePort(quoteApi);
    }

    @Bean("dummyJsonQuoteApi")
    public QuoteApi getQuoteApi(DummyJsonProperties dummyJsonProperties) {
        val apiClient = new ApiClient();
        apiClient.setBasePath(dummyJsonProperties.getUrl());
        QuoteApi apiInstance = new QuoteApi(apiClient);
        return apiInstance;
    }
}
