package com.sigli.alexandershelyugov.quoteservice.application.service.adapters.randomquote.dummyjson;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "integrations.dummyjson")
public class DummyJsonProperties {
    String url;
}
