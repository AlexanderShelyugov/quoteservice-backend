plugins {
    id("java")
    alias(libs.plugins.lombok.plugin)
}

group = "com.sigli.alexandershelyugov"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(project(":application:adapters"))
    implementation(libs.spring.boot.data.r2dbc)
}

tasks.withType<JavaCompile>().configureEach {
    options.encoding = "UTF-8"
}