package com.sigli.alexandershelyugov.quoteservice.application.service.adapters.randomquote.statham;

import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface StathamQuoteRepository extends ReactiveCrudRepository<StathamQuote, Integer> {
    @Query("SELECT * FROM STATHAM_QUOTE ORDER BY RAND() LIMIT 1;")
    Mono<StathamQuote> findRandomQuote();
}
