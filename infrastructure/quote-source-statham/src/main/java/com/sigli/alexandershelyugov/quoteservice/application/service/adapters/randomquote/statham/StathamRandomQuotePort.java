package com.sigli.alexandershelyugov.quoteservice.application.service.adapters.randomquote.statham;

import com.sigli.alexandershelyugov.quoteservice.application.service.adapters.randomquote.GetRandomQuotePort;
import com.sigli.alexandershelyugov.quoteservice.domain.model.Quote;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service("stathamRandomQuotePort")
@RequiredArgsConstructor
public class StathamRandomQuotePort implements GetRandomQuotePort {
    private final StathamQuoteRepository repository;

    @Override
    public Mono<Quote> getRandomQuote() {
        return repository
            .findRandomQuote()
            .map(stathamQuote -> new Quote(
                stathamQuote.getId().toString(),
                stathamQuote.getQuote(),
                "Джейсон Стетхем",
                "statham"
            ));
    }
}
