package com.sigli.alexandershelyugov.quoteservice.application.service.adapters.randomquote.statham;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StathamQuote {
    @Id
    private Integer id;
    private String quote;
}
