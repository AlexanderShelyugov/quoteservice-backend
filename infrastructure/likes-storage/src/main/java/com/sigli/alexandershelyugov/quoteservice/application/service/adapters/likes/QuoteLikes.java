package com.sigli.alexandershelyugov.quoteservice.application.service.adapters.likes;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QuoteLikes implements Persistable<String> {
    @Id
    private String id;
    private Integer likes;

    @Override
    public boolean isNew() {
        return id == null || likes == null || likes <= 0;
    }
}
