package com.sigli.alexandershelyugov.quoteservice.application.service.adapters.likes;

import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@RequiredArgsConstructor
public class LikesStorage implements LikesStoragePort {
    private final LikesRepository likesRepository;

    @Override
    public Mono<Integer> putLike(String id, String source) {
        // TODO validate that quote exists
        val likeId = "#" + id + "#" + source;
        return likesRepository.findById(likeId)
            .switchIfEmpty(likesRepository.save(new QuoteLikes(likeId, 0)))
            .flatMap(quoteLikes -> {
                val likes = quoteLikes.getLikes();
                quoteLikes.setLikes(likes + 1);
                return likesRepository.save(quoteLikes);
            })
            .map(QuoteLikes::getLikes);
    }
}
