package com.sigli.alexandershelyugov.quoteservice.application.service.adapters.likes;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface LikesRepository extends ReactiveCrudRepository<QuoteLikes, String> {
}
