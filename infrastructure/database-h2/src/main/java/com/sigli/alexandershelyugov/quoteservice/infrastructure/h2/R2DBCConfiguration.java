package com.sigli.alexandershelyugov.quoteservice.infrastructure.h2;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;

@Configuration
@EnableR2dbcRepositories(basePackages = "com.sigli.alexandershelyugov.quoteservice")
class R2DBCConfiguration {
}