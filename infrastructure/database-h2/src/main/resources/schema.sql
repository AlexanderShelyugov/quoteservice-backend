create table STATHAM_QUOTE(
  ID int not null AUTO_INCREMENT,
  QUOTE varchar(256) not null,
  PRIMARY KEY ( ID )
);

create table QUOTE_LIKES(
    ID varchar(256) not null,
    LIKES int not null DEFAULT 0,
    PRIMARY KEY (ID)
);