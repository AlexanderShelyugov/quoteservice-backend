plugins {
    id("java")
}

group = "com.sigli.alexandershelyugov"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(libs.spring.boot.data.r2dbc)
    runtimeOnly(libs.bundles.h2.runtime)
}
