package com.sigli.alexandershelyugov.quoteservice.application.service.adapters.randomquote.composite;

import com.sigli.alexandershelyugov.quoteservice.application.service.adapters.randomquote.GetRandomQuotePort;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.util.Collection;

@Configuration
@Qualifier
class CompositeRandomQuoteSpringConfiguration {
    private static final int RETRY_COUNT = 3;

    @Bean
    @Primary
    GetRandomQuotePort getCompositePort(Collection<GetRandomQuotePort> otherQuotePorts) {
        return new CompositeQuotePort(RETRY_COUNT, otherQuotePorts);
    }
}
