package com.sigli.alexandershelyugov.quoteservice.application.service.adapters.randomquote.composite;

import com.sigli.alexandershelyugov.quoteservice.application.service.adapters.randomquote.GetRandomQuotePort;
import com.sigli.alexandershelyugov.quoteservice.domain.model.Quote;
import reactor.core.publisher.Mono;

import java.util.Collection;
import java.util.List;
import java.util.Random;

public class CompositeQuotePort implements GetRandomQuotePort {
    private final int retryCount;
    private final List<GetRandomQuotePort> quoteSources;
    private final Random random = new Random();

    public CompositeQuotePort(int retryCount, Collection<GetRandomQuotePort> quoteSources) {
        if (quoteSources == null || quoteSources.isEmpty()) {
            throw new IllegalArgumentException("No sources to get quote from");
        }
        if (retryCount <= 0) {
            throw new IllegalArgumentException("Incorrect retryCount configured");
        }
        this.retryCount = retryCount;
        this.quoteSources = List.copyOf(quoteSources);
    }

    @Override
    public Mono<Quote> getRandomQuote() {
        int trying = retryCount;
        while (trying-- > 0) {
            try {
                final int sourceToGetFrom = random.nextInt(quoteSources.size());
                GetRandomQuotePort quotePort = quoteSources.get(sourceToGetFrom);
                return quotePort.getRandomQuote();
            } catch (Exception e) {
                // Nothing for now
            }
        }
        throw new IllegalStateException("Unable to fetch a quote, we tried");
    }
}
