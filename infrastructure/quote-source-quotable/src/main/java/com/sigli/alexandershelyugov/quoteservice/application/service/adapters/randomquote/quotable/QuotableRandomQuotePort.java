package com.sigli.alexandershelyugov.quoteservice.application.service.adapters.randomquote.quotable;

import com.sigli.alexandershelyugov.quoteservice.adapters.randomquote.quotable.api.QuoteApi;
import com.sigli.alexandershelyugov.quoteservice.application.service.adapters.randomquote.GetRandomQuotePort;
import com.sigli.alexandershelyugov.quoteservice.domain.model.Quote;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;


@RequiredArgsConstructor
public class QuotableRandomQuotePort implements GetRandomQuotePort {
    private final QuoteApi quoteApi;

    @Override
    public Mono<Quote> getRandomQuote() {
        return quoteApi.getRandomQuote()
            .map(q -> new Quote(
                q.getId(),
                q.getContent(),
                q.getAuthor(),
                "quotable"
            ));
    }
}
