package com.sigli.alexandershelyugov.quoteservice.application.service.adapters.randomquote.quotable;

import com.sigli.alexandershelyugov.quoteservice.adapters.randomquote.quotable.api.QuoteApi;
import com.sigli.alexandershelyugov.quoteservice.adapters.randomquote.quotable.invoker.ApiClient;
import com.sigli.alexandershelyugov.quoteservice.application.service.adapters.randomquote.GetRandomQuotePort;
import lombok.val;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class QuotableSpringConfiguration {
    @Bean("quoteableRandomQuotePort")
    GetRandomQuotePort getRandomQuotePort(QuoteApi quoteApi) {
        return new QuotableRandomQuotePort(quoteApi);
    }

    @Bean("quoteableQuoteApi")
    QuoteApi getQuoteApi(QuotableProperties quotableProperties) {
        val apiClient = new ApiClient();
        apiClient.setBasePath(quotableProperties.getUrl());
        QuoteApi apiInstance = new QuoteApi(apiClient);
        return apiInstance;
    }
}
