package com.sigli.alexandershelyugov.quoteservice.application.service.adapters.randomquote.quotable;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "integrations.quotable")
public class QuotableProperties {
    String url;
}
