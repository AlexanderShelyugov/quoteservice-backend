plugins {
    id("java")
    alias(libs.plugins.lombok.plugin)
    alias(libs.plugins.codegen.openapi)
}

group = "com.sigli.alexandershelyugov"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(project(":application:adapters"))
    implementation(libs.spring.context)
    implementation(libs.spring.web)
    implementation(libs.spring.webflux)
    implementation(libs.spring.boot)
    implementation(libs.bundles.jackson)
    implementation(libs.bundles.codegen.workaround) {
        because("It's a workaround for an outdated Mustache template from Spring generator")
    }
}

tasks.findByName("openApiGenerate")?.let {
    tasks.withType<JavaCompile>() {
        dependsOn("openApiGenerate")
    }
}

sourceSets {
    main {
        java {
            srcDir("$projectDir/build/generated/src/main/java")
        }
    }
}

openApiGenerate {
    // More information on the Generator configurations, refer to:
    // https://github.com/OpenAPITools/openapi-generator/tree/master/modules/openapi-generator-gradle-plugin#configuration
    // https://openapi-generator.tech/docs/generators/java/#config-options
    inputSpec.set("$projectDir/src/main/resources/openapi/schema-v3.0.json")
    outputDir.set("$projectDir/build/generated")
    cleanupOutput.set(true)
    ignoreFileOverride.set("$projectDir/.openapi-generator-ignore")

    val prefix ="${group}.quoteservice.adapters.randomquote.quotable"
    apiPackage.set("${prefix}.api")
    invokerPackage.set("${prefix}.invoker")
    modelPackage.set("${prefix}.model")
    modelNameSuffix.set("Response")

    generateModelTests.set(false)
    generateApiTests.set(false)

    generatorName.set("java")
    configOptions.set(mapOf(
            "library" to "webclient",
            "openApiNullable" to "false",
            "serializationLibrary" to "jackson",
    ))
}