package com.sigli.alexandershelyugov.quoteservice.infrastructure.entry_points.random_query.endpoints;

import com.sigli.alexandershelyugov.quoteservice.api.http.api.QuotesApi;
import com.sigli.alexandershelyugov.quoteservice.api.http.model.QuoteDto;
import com.sigli.alexandershelyugov.quoteservice.domain.usecase.randomquote.GetRandomQuoteUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
public class GetRandomQuoteHTTPEndpoint implements QuotesApi {
    private final GetRandomQuoteUseCase useCase;

    @Override
    public Mono<ResponseEntity<QuoteDto>> getRandomQuote(ServerWebExchange exchange) {
        return useCase.getRandomQuote()
            .map(quote -> new QuoteDto()
                .id(quote.id())
                .saying(quote.quote())
                .author(quote.author())
                .source(quote.source())
            )
            .map(ResponseEntity::ok);
    }
}
