package com.sigli.alexandershelyugov.quoteservice.infrastructure.entry_points.endpoints.likes;

import com.sigli.alexandershelyugov.quoteservice.api.http.api.LikesApi;
import com.sigli.alexandershelyugov.quoteservice.api.http.model.LikeDto;
import com.sigli.alexandershelyugov.quoteservice.domain.usecase.likes.PutLikeCommand;
import com.sigli.alexandershelyugov.quoteservice.domain.usecase.likes.PutLikeUseCase;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
public class LikesHTTPEndpoint implements LikesApi {
    private final PutLikeUseCase putLikeUseCase;

    @Override
    public Mono<ResponseEntity<LikeDto>> getLikes(String source, String id, ServerWebExchange exchange) {
        return null;
    }

    @Override
    public Mono<ResponseEntity<LikeDto>> putLikeOnQuote(String source, String id, ServerWebExchange exchange) {
        val command = new PutLikeCommand(source, id);
        return putLikeUseCase.putLike(command)
            .map(output -> new LikeDto()
                .id(output.id())
                .source(output.source())
                .likes(output.likes())
            )
            .map(ResponseEntity::ok);
    }
}
