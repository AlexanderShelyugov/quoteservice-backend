plugins {
    id("java")
    alias(libs.plugins.lombok.plugin)
}

group = "com.sigli.alexandershelyugov"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(project(":domain:usecase"))
    implementation(project(":presentation:quote-service-web-api"))
    implementation(libs.spring.webflux)
}
