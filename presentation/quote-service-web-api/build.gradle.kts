plugins {
    id("java")
    alias(libs.plugins.codegen.openapi)
}

group = "com.sigli.alexandershelyugov"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(libs.spring.doc)
    implementation(libs.bundles.codegen.workaround) {
        because("It's a workaround for an outdated Mustache template from the Spring generator")
    }
}

sourceSets {
    main {
        java {
            srcDir("$projectDir/build/generated/src/main/java")
        }
    }
}

tasks.findByName("openApiGenerate")?.let {
    tasks.withType<JavaCompile>() {
        dependsOn("openApiGenerate")
    }
}

openApiGenerate {
    // More information on the Generator configurations, refer to:
    // https://openapi-generator.tech/docs/generators/
    // https://github.com/OpenAPITools/openapi-generator/tree/master/modules/openapi-generator-gradle-plugin#configuration
    // https://openapi-generator.tech/docs/generators/java/#config-options
    // https://openapi-generator.tech/docs/generators/spring
    inputSpec.set("$projectDir/src/main/resources/openapi/schema-v3.0.json")
    outputDir.set("$projectDir/build/generated")
    cleanupOutput.set(true)

    val prefix ="${group}.quoteservice.api.http"
    apiPackage.set("${prefix}.api")
    invokerPackage.set("${prefix}.invoker")
    modelPackage.set("${prefix}.model")
    modelNameSuffix.set("Dto")

    generateModelTests.set(false)
    generateApiTests.set(false)

    generatorName.set("spring")
    configOptions.set(mapOf(
            "annotationLibrary" to "swagger2",
            "documentationProvider" to "springdoc",
            "openApiNullable" to "false",
            "interfaceOnly" to "true",
            "legacyDiscriminatorBehavior" to "false",
            "library" to "spring-boot",
            "reactive" to "true",
            "requestMappingMode" to "api_interface",
            "skipDefaultInterface" to "true",
            "title" to "Random Quotes API",
            "useBeanValidation" to "false",
            "useTags" to "true",
    ))
}
