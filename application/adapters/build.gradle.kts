plugins {
    id("java-library")
}

group = "com.sigli.alexandershelyugov"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    api(project(":domain:model"))
    api(libs.reactor.core)
}
