package com.sigli.alexandershelyugov.quoteservice.application.service.adapters.randomquote;

import com.sigli.alexandershelyugov.quoteservice.domain.model.Quote;
import reactor.core.publisher.Mono;

public interface GetRandomQuotePort {
    Mono<Quote> getRandomQuote();
}
