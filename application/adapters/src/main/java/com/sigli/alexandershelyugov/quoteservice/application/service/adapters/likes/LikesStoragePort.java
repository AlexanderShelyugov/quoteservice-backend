package com.sigli.alexandershelyugov.quoteservice.application.service.adapters.likes;

import reactor.core.publisher.Mono;

public interface LikesStoragePort {
    Mono<Integer> putLike(String id, String source);
}
