package com.sigli.alexandershelyugov.quoteservice.application.service.randomquote;

import com.sigli.alexandershelyugov.quoteservice.application.service.adapters.randomquote.GetRandomQuotePort;
import com.sigli.alexandershelyugov.quoteservice.domain.usecase.randomquote.GetRandomQuoteOutput;
import com.sigli.alexandershelyugov.quoteservice.domain.usecase.randomquote.GetRandomQuoteUseCase;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
public class RandomQuoteService implements GetRandomQuoteUseCase {
    private final GetRandomQuotePort quotePort;

    @Override
    public Mono<GetRandomQuoteOutput> getRandomQuote() {
        return quotePort.getRandomQuote()
            .map(quote -> new GetRandomQuoteOutput(
                quote.id(),
                quote.saying(),
                quote.author(),
                quote.source()
            ));
    }
}
