package com.sigli.alexandershelyugov.quoteservice.application.configuration;

import com.sigli.alexandershelyugov.quoteservice.application.service.adapters.likes.LikesStoragePort;
import com.sigli.alexandershelyugov.quoteservice.application.service.adapters.randomquote.GetRandomQuotePort;
import com.sigli.alexandershelyugov.quoteservice.application.service.likes.PutLikeService;
import com.sigli.alexandershelyugov.quoteservice.application.service.randomquote.RandomQuoteService;
import com.sigli.alexandershelyugov.quoteservice.domain.usecase.randomquote.GetRandomQuoteUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class ApplicationLayerConfiguration {
    @Bean
    GetRandomQuoteUseCase randomQuoteUseCase(GetRandomQuotePort quotePort) {
        return new RandomQuoteService(quotePort);
    }

    @Bean
    PutLikeService putLikeService(LikesStoragePort likesStoragePort) {
        return new PutLikeService(likesStoragePort);
    }
}
