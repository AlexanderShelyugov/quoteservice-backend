package com.sigli.alexandershelyugov.quoteservice.application.service.likes;

import com.sigli.alexandershelyugov.quoteservice.application.service.adapters.likes.LikesStoragePort;
import com.sigli.alexandershelyugov.quoteservice.domain.usecase.likes.PutLikeCommand;
import com.sigli.alexandershelyugov.quoteservice.domain.usecase.likes.PutLikeOutput;
import com.sigli.alexandershelyugov.quoteservice.domain.usecase.likes.PutLikeUseCase;
import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

@RequiredArgsConstructor
public class PutLikeService implements PutLikeUseCase {
    private final LikesStoragePort likesStoragePort;

    @Override
    public Mono<PutLikeOutput> putLike(PutLikeCommand command) {
        return likesStoragePort
            .putLike(command.id(), command.source())
            .map(result -> new PutLikeOutput(
                command.id(),
                command.source(),
                result
            ));
    }
}
