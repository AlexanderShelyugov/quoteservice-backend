# Quote Service Backend

The ready-made web-service, that provides you with quotes from various sources.

![Docker](https://img.shields.io/badge/Docker-2CA5E0?style=for-the-badge&logo=docker&logoColor=white)
![Spring Boot](https://img.shields.io/badge/Spring_Boot-6DB33F?style=for-the-badge&logo=spring-boot&logoColor=white)
![Spring WebFlux](https://img.shields.io/badge/WebFlux-6DB33F?style=for-the-badge&logo=spring&logoColor=white)
![OpenAPI](https://img.shields.io/badge/OpenAPI-6BA539?style=for-the-badge&logo=openapiinitiative&logoColor=white)
![Gradle](https://img.shields.io/badge/Gradle-02303A?style=for-the-badge&logo=gradle&logoColor=white)

---
## Getting Started
### Access
The Quote Service is deployed on [Render](https://render.com/).

You can access API Docs via https://quote-service-backend.onrender.com/api-docs

We also have a health actuator via [/actuator/health](https://quote-service-backend.onrender.com/actuator/health)

### Architecture
You can read more on the architecture decisions, taken here in [the specified documentation file](docs/ARCHITECTURE.md).

### Build
```shell
./gradlew bootRun
```
If you want to build a Docker image, I have also prepared a ready-made script in the root folder. Or just use the [Dockerfile](deployment/docker/Dockerfile).
```shell
bash ./build.sh
```
ℹ️ For web interaction, I use the code generation in this project. If you see some classes are "missing", just run an `assemble` or `openApiGenerate` task and they will appear. I'm working on making codegen automatic.

### Run
You can access Web Documentation via the url:

http://localhost:8080/api-docs
