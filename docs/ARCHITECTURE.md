# Explaining architecture decisions

> TL;DR - I use the best design practices, known by far

## Summary
So, long story short, I used [Clean Architecture](#clean-architecture), [Non-blocking IO](#non-blocking-io), and [Web API code generation](#webapi-code-generation).

## Clean Architecture
This it the best layout, I know so far. It is very scalable and business-logic oriented.
There are various posts about it, for example:
- [Hackney’s API Playbook explanation](https://lbhackney-it.github.io/API-Playbook/clean_architecture/)
- [Uncle Bob's take on Clean Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)
- [A very good article](https://www.dandoescode.com/blog/clean-architecture-an-introduction#presentation-layer)
- [Gradle solution of the Clean Architecture](https://github.com/bancolombia/scaffold-clean-architecture)

The key layout here is explained in this image:

<img src="img/clean-architecture.webp" width="50%" alt="Clean Architecture schema explained"/>

We have the [domain](/domain) layer. It contains domain objects, use cases and user stories. It is pure Java and MUST do nothing.

The [application](/application) layer implements use cases and defines what it needs in adapters. It MUST also be pure Java and contains no information on the framework details.

Inside [infrastructure](/infrastructure) layer we contain all the framework details, that are executed. They cannot do anything by themselves.

On the flip side, the [presentation](/presentation) layer contains the details that can execute use cases or other details.

Last but not least, the [deployment](/deployment) layer has everything we need to wire other layers together and configure the application.

You can examine the resulting [gradle.build.kts](/deployment/app/build.gradle.kts) file - dependencies can be included and excluded if needed, resulting in the flexible build variants.

## Non-Blocking IO
We use Spring [WebFlux](https://docs.spring.io/spring-framework/reference/web/webflux.html) and [R2DBC](https://spring.io/projects/spring-data-r2dbc) all the way. All connections between layers use [Project Reactor's](https://projectreactor.io/) [Mono and Flux](https://projectreactor.io/docs/core/release/reference/#core-features).

That way we vastly improve the service's performance!

## WebAPI code generation
We use OpenAPI [generator](https://github.com/OpenAPITools/openapi-generator) to generate client code classes and server stubs with a ready-made [JSON specs](https://swagger.io/docs/specification/basic-structure/).

This saves the development time and ensures connectivity between our service and others.

To generate necessary classes, run `assemble`, `build` or `openApiGenerate` tasks.
