package com.sigli.alexandershelyugov.quoteservice.domain.model;

public record Quote(
    String id,
    String saying,
    String author,
    String source
) {
}
