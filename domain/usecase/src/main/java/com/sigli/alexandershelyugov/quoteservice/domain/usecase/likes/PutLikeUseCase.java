package com.sigli.alexandershelyugov.quoteservice.domain.usecase.likes;

import reactor.core.publisher.Mono;

public interface PutLikeUseCase {
    Mono<PutLikeOutput> putLike(PutLikeCommand command);
}
