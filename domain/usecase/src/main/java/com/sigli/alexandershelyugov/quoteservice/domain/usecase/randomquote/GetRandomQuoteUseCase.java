package com.sigli.alexandershelyugov.quoteservice.domain.usecase.randomquote;

import reactor.core.publisher.Mono;

public interface GetRandomQuoteUseCase {
    Mono<GetRandomQuoteOutput> getRandomQuote();
}
