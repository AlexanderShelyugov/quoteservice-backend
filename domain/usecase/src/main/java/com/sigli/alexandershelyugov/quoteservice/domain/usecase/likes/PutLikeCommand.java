package com.sigli.alexandershelyugov.quoteservice.domain.usecase.likes;

public record PutLikeCommand(
    String source,
    String id
) {
}
