package com.sigli.alexandershelyugov.quoteservice.domain.usecase.likes;

public record PutLikeOutput(
    String id,
    String source,
    Integer likes
) {
}
