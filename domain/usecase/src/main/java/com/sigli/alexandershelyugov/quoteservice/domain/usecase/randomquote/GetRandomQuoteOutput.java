package com.sigli.alexandershelyugov.quoteservice.domain.usecase.randomquote;

public record GetRandomQuoteOutput(
    String id,
    String quote,
    String author,
    String source
) {
}
