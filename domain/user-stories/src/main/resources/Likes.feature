Feature: Likes for quotes
  As a user, I want to put likes to quotes

  Background:
    User is a random person

  Scenario: Put a like
    Given there is an existing quote
    And the quote had 100 likes already
    When User puts a like on the quote
    Then the quote has 101 likes

  Scenario: Get likes
    Given there is an existing quote
    And the quote had 102 likes already
    When User gets the amount of likes for the quote
    Then the quote has 102 likes