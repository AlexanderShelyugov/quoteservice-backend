Feature: Retrieving quotes
  As a user I want to be able to get quotes

  Background:
    User is a random person

  Scenario: Quote retrieval
    When User asks our system for a quote
    Then User gets a random quote
