Feature: Basic stable running
  As a user I want to be sure that the application is at least running

  Background:
    User is a random person

  Scenario: Application start
    Given we start the application
    When we check if the application is running
    Then the application runs just fine