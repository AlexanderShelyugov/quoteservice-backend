plugins {
    id("java")
    alias(libs.plugins.spring.boot)
}

group = "com.sigli.alexandershelyugov"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

tasks.compileJava {
    options.release = 21
}

springBoot {
    tasks.bootJar {
        archiveFileName.set("quote-service-backend.jar")
    }
}

dependencies {
    implementation(project(":application:services"))

    implementation(project(":infrastructure:database-h2"))
    implementation(project(":infrastructure:likes-storage"))
    implementation(project(":infrastructure:quote-source-composite"))
    implementation(project(":infrastructure:quote-source-dummyjson"))
    implementation(project(":infrastructure:quote-source-quotable"))
    implementation(project(":infrastructure:quote-source-statham"))

    implementation(project(":presentation:health-actuator"))
    implementation(project(":presentation:likes-api"))
    implementation(project(":presentation:random-query-web"))

    implementation(libs.spring.boot)
}
