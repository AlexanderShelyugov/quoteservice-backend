package com.sigli.alexandershelyugov.quoteservice.presentation.application.configuration;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = "com.sigli.alexandershelyugov.quoteservice")
public class GlobalAppConfiguration {
}
