package com.sigli.alexandershelyugov.quoteservice.presentation.application;

import com.sigli.alexandershelyugov.quoteservice.presentation.application.configuration.GlobalAppConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(GlobalAppConfiguration.class)
public class QuoteServiceBackendApp {
    public static void main(String[] args) {
        SpringApplication.run(QuoteServiceBackendApp.class);
    }
}
